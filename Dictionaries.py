#!/usr/bin/env python
"""Dictionaries"""
from collections import defaultdict

fake_database = {"house": [], 1: "one two three"}

for el, value in fake_database.items():
    print(el)
    print(value)

print(fake_database[1])

fake_database_2 = defaultdict(int)

print(fake_database_2[1])


str = "asd asd adsad as d asdas das. ddd dada sdsad. "
fake_database_3 = defaultdict(int)

for word in str.split(" "):
    fake_database_3[word] += 1

for k, v in fake_database_3.items():
    print(k, v)
