#!/usr/bin/env python
"""Exceptions - the evil """


class DividerDivisionError(ZeroDivisionError): pass


def divide(a: type(int), b: type(int)):
    if b == 0:
        raise DividerDivisionError("Putted 0 as a param B")
    return a/b


print(divide(11, 5))
try:
    print(divide(10, 2))
except ZeroDivisionError:
    print("ZeroDivErr")
else:
    print("Else after divide 10 by 2")
finally:
    print("You divided 10 by 2")


try:
    print(divide(2, 0))
except ZeroDivisionError as zde:
    print("Exception was found")
except ValueError as ve:
    print(ve)
except DividerDivisionError as dde:
    print(dde)
